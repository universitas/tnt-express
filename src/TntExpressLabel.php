<?php

namespace Universitas\TntExpress;

use DateTime;
use Universitas\TntExpress\Elements\Account;
use Universitas\TntExpress\Elements\Address;
use Universitas\TntExpress\Elements\CollectionDateTime;
use Universitas\TntExpress\Elements\ConsignmentIdentity;
use Universitas\TntExpress\Elements\OptionalElements;
use Universitas\TntExpress\Elements\PieceLine;
use Universitas\TntExpress\Elements\PieceMeasurements;
use Universitas\TntExpress\Elements\Pieces;
use Universitas\TntExpress\Elements\Product;
use Universitas\TntExpress\Elements\TotalNumberOfPieces;

class TntExpressLabel extends TntExpress
{
    public $url = 'https://express.tnt.com/expresslabel/documentation/getlabel';

    public function __construct($userId, $password, $url = null)
    {
        if (!is_null($url)) {
            $this->url = $url;
        }
        parent::__construct($userId, $password, $this->url);
    }

    public function setSender(string $name, string $addressLine1, string $addressLine2 = null, string $addressLine3 = null,
                              string $town, string $exactMatch = null, string $province = null, string $postcode = null, string $country): TntExpressLabel
    {
        $sender = new Address();
        $sender->setName($name)
            ->setAddressLine1($addressLine1)
            ->setAddressLine2($addressLine2)
            ->setAddressLine3($addressLine3)
            ->setTown($town)
            ->setExactMatch($exactMatch)
            ->setProvince($province)
            ->setPostcode($postcode)
            ->setCountry($country);
        $this->createElement("sender", $sender);

        return $this;
    }

    public function setDelivery(string $name, string $addressLine1, string $addressLine2 = null, string $addressLine3 = null,
                                string $town, string $exactMatch = null, string $province = null, string $postcode = null, string $country): TntExpressLabel
    {
        $delivery = new Address();
        $delivery->setName($name)
            ->setAddressLine1($addressLine1)
            ->setAddressLine2($addressLine2)
            ->setAddressLine3($addressLine3)
            ->setTown($town)
            ->setExactMatch($exactMatch)
            ->setProvince($province)
            ->setPostcode($postcode)
            ->setCountry($country);
        $this->createElement("delivery", $delivery);

        return $this;
    }

    public function setConsignementIdentity(string $consignmentNumber, string $customerReference): TntExpressLabel
    {
        $consignmentIdentity = new ConsignmentIdentity();
        $consignmentIdentity
            ->setConsignmentNumber($consignmentNumber)
            ->setCustomerReference($customerReference);
        $this->createElement("consignmentIdentity", $consignmentIdentity);

        return $this;
    }

    public function setCollectionDateTime(DateTime $collectionDateTime): TntExpressLabel
    {
        $object = new CollectionDateTime();
        $object->setCollectionDateTime($collectionDateTime);
        $this->xml->writeRaw($object->getAsXml());

        return $this;
    }

    public function setProduct(string $lineOfBusiness, string $groupId, string $subGroupId, string $id, string $type, string $option = null): TntExpressLabel
    {
        $product = new Product();
        $product->setLineOfBusiness($lineOfBusiness)
            ->setGroupId($groupId)
            ->setSubGroupId($subGroupId)
            ->setId($id)
            ->setType($type)
            ->setOption($option);
        $this->createElement("product", $product);

        return $this;
    }

    public function setAccount(string $accountNumber, string $accountCountry): TntExpressLabel
    {
        $account = new Account();
        $account->setAccountNumber($accountNumber)
            ->setAccountCountry($accountCountry);
        $this->createElement("account", $account);

        return $this;
    }

    public function setTotalNumberOfPieces(int $totalNumberOfPieces): TntExpressLabel
    {
        $object = new TotalNumberOfPieces();
        $object->setTotalNumberOfPieces($totalNumberOfPieces);
        $this->xml->writeRaw($object->getAsXml());

        return $this;
    }

    /**
     * @param string $identifier
     * @param string $goodsDescription
     * @param string $length
     * @param string $width
     * @param string $height
     * @param string $weight
     * @param array|Pieces[] $pieces
     * @return $this
     */
    public function setPieceLine(string $identifier, string $goodsDescription, string $length, string $width, string $height, string $weight, array $pieces): TntExpressLabel
    {
        $object = new PieceLine();
        $object
            ->setIdentifier($identifier)
            ->setGoodsDescription($goodsDescription)
            ->setPieceMeasurements((new PieceMeasurements())
                ->setLength($length)
                ->setWidth($width)
                ->setHeight($height)
                ->setWeight($weight)
            )
            ->setPieces($pieces);

        $this->createElement("pieceLine", $object);

        return $this;
    }

    public function setOptionalElements(string $bulkShipment = null, string $specialInstructions = null, string $cashAmount = null, string $cashCurrency = null, string $cashType = null, string $customControlled = null, string $termsOfPayment = null): TntExpressLabel
    {
        $optionalElements = new OptionalElements();
        $optionalElements->setBulkShipment($bulkShipment)
            ->setSpecialInstructions($specialInstructions)
            ->setCashAmount($cashAmount)
            ->setCashCurrency($cashCurrency)
            ->setCashType($cashType)
            ->setCustomControlled($customControlled)
            ->setTermsOfPayment($termsOfPayment);
        $this->xml->getAsXml();

        return $this;
    }
}
