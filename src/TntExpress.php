<?php

namespace Universitas\TntExpress;

use DOMDocument;
use XSLTProcessor;

abstract class TntExpress
{
    private $errorCode = 0;
    private $errorMessage = "";
    private $socketResponse = "";
    private $userId;
    private $password;
    private $url;

    /** @var XmlWriterOverride */
    protected $xml;

    public function __construct($userId, $password, $url)
    {
        $this->userId = $userId;
        $this->password = $password;
        $this->url = $url;
        $this->initXml();
    }

    public function initXml()
    {
        $this->xml = new XmlWriterOverride();
        $this->xml->openMemory();
        $this->xml->setIndent(true);
    }

    public function httpPost($strRequest)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $userPass = "";
        if ((trim($this->userId) != "") && (trim($this->password) != "")) {
            $userPass = $this->userId . ":" . $this->password;
            curl_setopt($ch, CURLOPT_USERPWD, $userPass);
        }
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $strRequest);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        $isSecure = strpos($this->url, "https://");

        if ($isSecure === 0) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        }
        $result = curl_exec($ch);
        $this->errorCode = curl_errno($ch);
        $this->errorMessage = curl_error($ch);
        $this->socketResponse = $result;
        curl_close($ch);
    }

    public function renderHtml($xml, $cssDir = null, $imgDir = null, $baseHref = null)
    {
        if (is_null($cssDir)) {
            $cssDir = "resources/css/";
        }
        if (is_null($imgDir)) {
            $imgDir = "resources/images/";
        }
        if (is_null($baseHref)) {
            $baseHref = __DIR__ . '/';
        }

        $domXsl = new DOMDocument();
        $domXsl->load(__DIR__ . '/resources/views/HTMLRoutingLabelRenderer.xsl');

        $xslt = new XSLTProcessor();
        $xslt->importStyleSheet($domXsl);
        $xslt->setParameter('', 'css_dir', $cssDir);
        $xslt->setParameter('', 'images_dir', $imgDir);
        $xslt->setParameter('', 'base_href', $baseHref);

        $domXml = new DOMDocument();
        $domXml->loadXML($xml);

        return $xslt->transformToXML($domXml);
    }

    public function startDocument()
    {
        $this->xml->startDocument("1.0", "UTF-8");
    }

    public function createElement($element, $object)
    {
        $this->xml->startElement($element);
        $this->xml->writeRaw($object->getAsXml());
        $this->xml->endElement();
    }

    public function flush()
    {
        return $this->xml->flush();
    }

    /**
     * Get the value of errorCode
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * Set the value of errorCode
     *
     * @return  self
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;

        return $this;
    }

    /**
     * Get the value of errorMessage
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * Set the value of errorMessage
     *
     * @return  self
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;

        return $this;
    }

    /**
     * Get the value of socketResponse
     */
    public function getSocketResponse()
    {
        return $this->socketResponse;
    }

    /**
     * Set the value of socketResponse
     *
     * @return  self
     */
    public function setSocketResponse($socketResponse)
    {
        $this->socketResponse = $socketResponse;

        return $this;
    }

    /**
     * Get the value of userId
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set the value of userId
     *
     * @return  self
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get the value of password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @return  self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }
}
