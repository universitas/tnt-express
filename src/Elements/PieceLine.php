<?php

namespace Universitas\TntExpress\Elements;

use Universitas\TntExpress\Elements\AbstractXml;

class PieceLine extends AbstractXml
{
    /**
     * @var string
     * Element is required
     */
    public $identifier;

    /**
     * @var string
     * Element is required
     */
    public $goodsDescription;

    /**
     * @var PieceMeasurements
     * Element is required
     */
    public $pieceMeasurements;

    /**
     * @var array|Pieces[]
     * Element is required
     */
    public $pieces;

    /**
     * Get element is required
     *
     * @return  string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set element is required
     *
     * @param string $identifier Element is required
     *
     * @return  self
     */
    public function setIdentifier(string $identifier)
    {
        $this->identifier = $identifier;
        $this->xml->writeElementCData('identifier', $identifier);

        return $this;
    }

    /**
     * Get element is required
     *
     * @return  string
     */
    public function getGoodsDescription()
    {
        return $this->goodsDescription;
    }

    /**
     * Set element is required
     *
     * @param string $goodsDescription Element is required
     *
     * @return  self
     */
    public function setGoodsDescription(string $goodsDescription)
    {
        $this->goodsDescription = $goodsDescription;
        $this->xml->writeElementCData('goodsDescription', $goodsDescription);

        return $this;
    }

    /**
     * Get element is required
     *
     * @return PieceMeasurements
     */
    public function getPieceMeasurements()
    {
        return $this->pieceMeasurements;
    }

    /**
     * Set element is required
     *
     * @param PieceMeasurements $pieceMeasurements
     * @return  self
     */
    public function setPieceMeasurements(PieceMeasurements $pieceMeasurements)
    {
        $this->pieceMeasurements = $pieceMeasurements;
        $this->xml->startElement("pieceMeasurements");
        $this->xml->writeRaw($pieceMeasurements->getAsXml());
        $this->xml->endElement();

        return $this;
    }

    /**
     * @return array|Pieces[]
     */
    public function getPieces(): array
    {
        return $this->pieces;
    }

    /**
     * @param array|Pieces[] $pieces
     */
    public function setPieces(array $pieces)
    {
        $this->pieces = $pieces;

        foreach ($pieces as $piece) {
            $this->xml->startElement("pieces");
            $this->xml->writeRaw($piece->getAsXml());
            $this->xml->endElement();
        }

        return $this;
    }
}
