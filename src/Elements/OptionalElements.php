<?php

namespace Universitas\TntExpress\Elements;

use Universitas\TntExpress\Elements\AbstractXml;

class OptionalElements extends AbstractXml
{
    /**
     * @var string
     * Element is optional
     */
    public $bulkShipment;

    /**
     * @var string
     * Element is optional
     */
    public $specialInstructions;

    /**
     * @var string
     * Element is optional
     */
    public $cashAmount;

    /**
     * @var string
     * Element is optional
     */
    public $cashCurrency;

    /**
     * @var string
     * Element is optional
     */
    public $cashType;

    /**
     * @var string
     * Element is optional
     */
    public $customControlled;

    /**
     * @var string
     * Element is optional
     */
    public $termsOfPayment;


    /**
     * Get element is optional
     *
     * @return  string
     */
    public function getBulkShipment()
    {
        return $this->bulkShipment;
    }

    /**
     * Set element is optional
     *
     * @param string $bulkShipment Element is optional
     *
     * @return  self
     */
    public function setBulkShipment(?string $bulkShipment)
    {
        $this->bulkShipment = $bulkShipment;
        $this->xml->writeElementCData('bulkShipment', $this->bulkShipment);
        return $this;
    }

    /**
     * Get element is optional
     *
     * @return  string
     */
    public function getSpecialInstructions()
    {
        return $this->specialInstructions;
    }

    /**
     * Set element is optional
     *
     * @param string $specialInstructions Element is optional
     *
     * @return  self
     */
    public function setSpecialInstructions(?string $specialInstructions)
    {
        $this->specialInstructions = $specialInstructions;
        $this->xml->writeElementCData('specialInstructions', $this->specialInstructions);
        return $this;
    }

    /**
     * Get element is optional
     *
     * @return  string
     */
    public function getCashAmount()
    {
        return $this->cashAmount;
    }

    /**
     * Set element is optional
     *
     * @param string $cashAmount Element is optional
     *
     * @return  self
     */
    public function setCashAmount(?string $cashAmount)
    {
        $this->cashAmount = $cashAmount;
        $this->xml->writeElementCData('cashAmount', $this->cashAmount);
        return $this;
    }

    /**
     * Get element is optional
     *
     * @return  string
     */
    public function getCashCurrency()
    {
        return $this->cashCurrency;
    }

    /**
     * Set element is optional
     *
     * @param string $cashCurrency Element is optional
     *
     * @return  self
     */
    public function setCashCurrency(?string $cashCurrency)
    {
        $this->cashCurrency = $cashCurrency;
        $this->xml->writeElementCData('cashCurrency', $this->cashCurrency);
        return $this;
    }

    /**
     * Get element is optional
     *
     * @return  string
     */
    public function getCashType()
    {
        return $this->cashType;
    }

    /**
     * Set element is optional
     *
     * @param string $cashType Element is optional
     *
     * @return  self
     */
    public function setCashType(?string $cashType)
    {
        $this->cashType = $cashType;
        $this->xml->writeElementCData('cashType', $this->cashType);
        return $this;
    }

    /**
     * Get element is optional
     *
     * @return  string
     */
    public function getCustomControlled()
    {
        return $this->customControlled;
    }

    /**
     * Set element is optional
     *
     * @param string $customControlled Element is optional
     *
     * @return  self
     */
    public function setCustomControlled(?string $customControlled)
    {
        $this->customControlled = $customControlled;
        $this->xml->writeElementCData('customControlled', $this->customControlled);
        return $this;
    }

    /**
     * Get element is optional
     *
     * @return  string
     */
    public function getTermsOfPayment()
    {
        return $this->termsOfPayment;
    }

    /**
     * Set element is optional
     *
     * @param string $termsOfPayment Element is optional
     *
     * @return  self
     */
    public function setTermsOfPayment(?string $termsOfPayment)
    {
        $this->termsOfPayment = $termsOfPayment;
        $this->xml->writeElementCData('termsOfPayment', $this->termsOfPayment);
        return $this;
    }
}
