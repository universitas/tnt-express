<?php

namespace Universitas\TntExpress\Elements;

use Universitas\TntExpress\Elements\AbstractXml;

class Pieces extends AbstractXml
{
    /**
     * @var string
     * Element is required
     */
    public $sequenceNumbers;

    /**
     * @var string
     * Element is required
     */
    public $pieceReference;

    /**
     * Get element is required
     *
     * @return  string
     */
    public function getSequenceNumbers()
    {
        return $this->sequenceNumbers;
    }

    /**
     * Set element is required
     *
     * @param string $sequenceNumbers Element is required
     *
     * @return  self
     */
    public function setSequenceNumbers(string $sequenceNumbers)
    {
        $this->sequenceNumbers = $sequenceNumbers;
        $this->xml->writeElementCData('sequenceNumbers', $sequenceNumbers);

        return $this;
    }

    /**
     * Get element is required
     *
     * @return  string
     */
    public function getPieceReference()
    {
        return $this->pieceReference;
    }

    /**
     * Set element is required
     *
     * @param string $pieceReference Element is required
     *
     * @return  self
     */
    public function setPieceReference(string $pieceReference)
    {
        $this->pieceReference = $pieceReference;
        $this->xml->writeElementCData('pieceReference', $pieceReference);

        return $this;
    }
}
