<?php

namespace Universitas\TntExpress\Elements;

use Universitas\TntExpress\Elements\AbstractXml;

class Product extends AbstractXml
{
    /**
     * @var string
     * Element is required
     */
    public $lineOfBusiness;

    /**
     * @var string
     * Element is required
     */
    public $groupId;

    /**
     * @var string
     * Element is required
     */
    public $subGroupId;

    /**
     * @var string
     * Element is required
     */
    public $id;

    /**
     * @var string
     * Element is required
     */
    public $type;

    /**
     * @var string
     * Element is optional
     */
    public $option;


    /**
     * Get element is required
     *
     * @return  string
     */
    public function getLineOfBusiness()
    {
        return $this->lineOfBusiness;
    }

    /**
     * Set element is required
     *
     * @param string $lineOfBusiness Element is required
     *
     * @return  self
     */
    public function setLineOfBusiness(string $lineOfBusiness)
    {
        $this->lineOfBusiness = $lineOfBusiness;
        $this->xml->writeElementCData('lineOfBusiness', $this->lineOfBusiness);
        return $this;
    }

    /**
     * Get element is required
     *
     * @return  string
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * Set element is required
     *
     * @param string $groupId Element is required
     *
     * @return  self
     */
    public function setGroupId(string $groupId)
    {
        $this->groupId = $groupId;
        $this->xml->writeElementCData('groupId', $this->groupId);
        return $this;
    }

    /**
     * Get element is required
     *
     * @return  string
     */
    public function getSubGroupId()
    {
        return $this->subGroupId;
    }

    /**
     * Set element is required
     *
     * @param string $subGroupId Element is required
     *
     * @return  self
     */
    public function setSubGroupId(string $subGroupId)
    {
        $this->subGroupId = $subGroupId;
        $this->xml->writeElementCData('subGroupId', $this->subGroupId);
        return $this;
    }

    /**
     * Get element is required
     *
     * @return  string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set element is required
     *
     * @param string $id Element is required
     *
     * @return  self
     */
    public function setId(string $id)
    {
        $this->id = $id;
        $this->xml->writeElementCData('id', $this->id);
        return $this;
    }

    /**
     * Get element is required
     *
     * @return  string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set element is required
     *
     * @param string $type Element is required
     *
     * @return  self
     */
    public function setType(string $type)
    {
        $this->type = $type;
        $this->xml->writeElementCData('type', $this->type);
        return $this;
    }

    /**
     * Get element is optional
     *
     * @return  string
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * Set element is optional
     *
     * @param string $option Element is optional
     *
     * @return  self
     */
    public function setOption(?string $option)
    {
        $this->option = $option;
        $this->xml->writeElementCData('option', $this->option);
        return $this;
    }
}
