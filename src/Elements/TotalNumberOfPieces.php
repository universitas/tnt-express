<?php

namespace Universitas\TntExpress\Elements;

class TotalNumberOfPieces extends AbstractXml
{
    /**
     * @var string
     * Element is required
     */
    public $totalNumberOfPieces;

    /**
     * Get element is required
     *
     * @return  string
     */
    public function getTotalNumberOfPieces()
    {
        return $this->totalNumberOfPieces;
    }

    /**
     * Set element is required
     *
     * @param int $totalNumberOfPieces Element is required
     *
     * @return  self
     */
    public function setTotalNumberOfPieces(int $totalNumberOfPieces)
    {
        $this->totalNumberOfPieces = $totalNumberOfPieces;
        $this->xml->writeElementCData('totalNumberOfPieces', $this->totalNumberOfPieces);

        return $this;
    }
}
