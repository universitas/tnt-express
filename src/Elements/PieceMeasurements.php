<?php

namespace Universitas\TntExpress\Elements;

use Universitas\TntExpress\Elements\AbstractXml;

class PieceMeasurements extends AbstractXml
{
    /**
     * @var string
     * Element is required
     */
    public $length;

    /**
     * @var string
     * Element is required
     */
    public $width;

    /**
     * @var string
     * Element is required
     */
    public $height;

    /**
     * @var string
     * Element is required
     */
    public $weight;

    /**
     * Get element is required
     *
     * @return  string
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set element is required
     *
     * @param string $length Element is required
     *
     * @return  self
     */
    public function setLength(string $length)
    {
        $this->length = $length;
        $this->xml->writeElementCData('length', $length);

        return $this;
    }

    /**
     * Get element is required
     *
     * @return  string
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set element is required
     *
     * @param string $width Element is required
     *
     * @return  self
     */
    public function setWidth(string $width)
    {
        $this->width = $width;
        $this->xml->writeElementCData('width', $width);

        return $this;
    }

    /**
     * Get element is required
     *
     * @return  string
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set element is required
     *
     * @param string $height Element is required
     *
     * @return  self
     */
    public function setHeight(string $height)
    {
        $this->height = $height;
        $this->xml->writeElementCData('height', $height);

        return $this;
    }

    /**
     * Get element is required
     *
     * @return  string
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set element is required
     *
     * @param string $weight Element is required
     *
     * @return  self
     */
    public function setWeight(string $weight)
    {
        $this->weight = $weight;
        $this->xml->writeElementCData('weight', $weight);

        return $this;
    }
}
