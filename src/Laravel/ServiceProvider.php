<?php

namespace Universitas\TntExpress\Laravel;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function register()
    {
        //
    }

    public function boot()
    {
        $this->publishes([__DIR__ . '/../resources' => $this->app->resourcePath('vendor/universitas/tnt-express')], 'resources');
    }
}
